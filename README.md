[![crates.io](https://img.shields.io/crates/v/char-buf)](https://crates.io/crates/char-buf)
[![docs.rs](https://img.shields.io/docsrs/char-buf)](https://docs.rs/char-buf)
[![pipeline status](https://gitlab.com/kkitahara/char-buf-rs/badges/main/pipeline.svg)](https://gitlab.com/kkitahara/char-buf-rs/-/pipelines)
[![coverage report](https://gitlab.com/kkitahara/char-buf-rs/badges/main/coverage.svg)](https://kkitahara.gitlab.io/char-buf-rs/coverage/)

# CharBuf

A writable, fixed-length `char` buffer usable in `no_std` environments.

```rust
use char_buf::CharBuf;
use core::fmt::Write;

// `CharBuf` with capacity `8`
type CharBuf8 = CharBuf<8>;

let mut w = CharBuf8::new();
write!(w, "x{:?}x", [1, 2]).unwrap();

assert_eq!(w, "x[1, 2]x");
```

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.

---

&copy; 2023&ndash;2024 Koichi Kitahara
