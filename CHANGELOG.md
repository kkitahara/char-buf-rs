# Change log

## v0.1.2 - 2024.02.12
* Fixed Eq impl.

## v0.1.1 - 2023.10.06
* Fixed documents.

## v0.1.0 - 2023.10.06
* Initial release.
